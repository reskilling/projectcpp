#include "Client.h" 
#include "MonException.h"
      
Client::Client(int I, string N, string L, string CM, int CD, string V, string E)
{
  setIdentifiant(I);
  setNom(N);
  AdressePostale = new Adresse(L,CM, CD, V);
  setEmail(E);
}

Client::~Client()
{
    cout << "Constructor (Client) : " << endl;
    if (AdressePostale != nullptr)
      {
        delete AdressePostale;
      }
}
   
void Client::setNom(string dNom)
{
    if (dNom.length() > 50)
    throw MonException(1);
    Nom = dNom; 
}

void Client::Infos()
{

   cout << "Identifiant : " << getIdentifiant() << endl;
   cout << "NOM : " << getNom() << endl;
   cout << (*AdressePostale).Infos();
   cout << "Email : " << getEmail() << endl;
}

Adresse Client::getAdressePostale()
{
   return *AdressePostale;
}

void Client::setAdressePostale(string newL, string newCM, int newCD, string newV)
{
   (*AdressePostale).setLibelle(newL);
   (*AdressePostale).setComplement(newCM);
   (*AdressePostale).setCode(newCD);
   (*AdressePostale).setVille(newV);
}