#include <iostream>
#include "Client.h"
#include "Particulier.h"
#include "Professionnel.h"
#include "MonException.h"
#include "gestionClient.h"
#include "Adresse.h"
#include "mainwindow.h"
#include <QApplication>

using namespace std;

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
/*
    try
    {
        gestionClient gC;

       //    cout << endl << "---------- Client Particulier ----------" << endl;

        Particulier p1(1, "BETY", "12 Rue des Oliviers", " ", 94000, "CRETEIL", "bety@gmail.com", 12, 11, 1985, "Daniel", 'M');
        Particulier p2(3, "BODIN", "10 rue des Olivies", "Etage 2", 94300, "VINCENNES", "bodin@gmail.com", 05, 05, 1965, "Justin", 'M');
        Particulier p3(5, "BERRIS", "15 rue de la République", " ", 94120, "FONTENAY SOUS BOIS", "berris@gmail.com", 06, 06, 1977, "Karine", 'F');
        Particulier p4(7, "ABENIR", "25 rue de la Paix", " ", 92100, "LA DEFENSE", "abenir@gmail.com", 12, 04, 1977, "Alexandra", 'F');
        Particulier p5(9, "BENSAID", "3, avenue des Parcs", " ", 93500, "ROISSY EN France", "bensaid@gmail.com", 16, 04, 1976, "Georgia", 'F');
        Particulier p6(11, "ABABOU", "3, rue Lecourbe", " ", 93200, "BAGNOLET", "ababou@gmail.com", 10, 10, 1970, "Teddy", 'M');

        //   cout << endl << "---------- Client Professionnel ----------" << endl;

        Professionnel s1(2, "AXA", "125, rue La Fayette", "Digicode 1432", 94120, "FONTENAY SOUS BOIS", "info@axa.fr", "12548795641122", "SARL", "125, rue La Fayette", "Digicode 1432", 94120, "FONTENAY SOUS BOIS");
        Professionnel s2(4, "PAUL", "36, quai des Orfèvres", " ", 93500, "ROISSY EN France", "info@paul.fr", "87459564455444", "EURL", "10, esplanade de la Défense", " ", 92060, "LA DEFENSE");
        Professionnel s3(6, "PRIMARK", "32, rue E. Renan", "Batiment C", 75002, "PARIS", "contact@primark.fr", "08755897458455", "SARL", "32, rue E. Renan", "Batiment C", 75002, "PARIS");
        Professionnel s4(8, "ZARA", "23, av P. Valery", " ", 92100, "LA DEFENSE", "info@zara.fr", "65895874587854", "SA", "24, esplanade de la Défense", "Tour Franklin", 92060, "LA DEFENSE");
        Professionnel s5(10, "LEONIDAS", "15, Place de la Bastille", "Fond de Cour", 75003, "PARIS", "contact@leonidas.fr", "91235987456832", "SAS", "10 Rue de la Paix", " ", 75008, "PARIS");

        //Ajout des clients

        gC.Ajouter(&p1);
        gC.Ajouter(&p2);
        gC.Ajouter(&p3);
        gC.Ajouter(&p4);
        gC.Ajouter(&p5);
        gC.Ajouter(&p6);
        gC.Ajouter(&s1);
        gC.Ajouter(&s2);
        gC.Ajouter(&s3);
        gC.Ajouter(&s4);
        gC.Ajouter(&s5);

        gC.AfficherTout();
        cout << endl << "#############################" << endl;
    }
    catch(MonException& ex)
    {
    //    cout << "MonException : " <<  ex.Getmessage() << endl;
        cout << "MonException(what) : " <<  ex.what() << endl;
    }

    return 0;
    */
}
