#include "gestionClient.h"

gestionClient::gestionClient()
{
//     vecClient.clear();
    mapClient.clear();
}

gestionClient::~gestionClient()
{
    cout << "Destructor (Gestion CLient) " << endl;
/*
    for (int i(0) ; i < GetnbClient(); i++ )
    {
      vecClient[i] = nullptr;

    }
*/
    mapClient.clear();    
}

void gestionClient::Ajouter(Client* ClientToAdd)
{
//    vecClient.push_back(ClientToAdd);
        mapClient.insert(pair<int, Client*>(ClientToAdd->getIdentifiant(), ClientToAdd));
}

void gestionClient::AfficherTout()
{
/*
    for (int i(0) ; i < vecClient.size() ; i++ )
    {
        cout << endl << "---------------------------------------" << endl;
        vecClient[i]->Infos();
        cout << endl << "----------------------------------------" << endl;
    }
*/
    map<int, Client*>::iterator itr;

     for (itr = mapClient.begin(); itr != mapClient.end(); itr++)
         {
           // cout << itr->first << itr->second << endl; 
            itr->second->Infos();
            cout << "-------------------------" << endl;
         }
}

int gestionClient::GetnbClient()
{
//    return vecClient.size();
    return mapClient.size();
}

Client* gestionClient::getClient(int position)
{
//    return vecClient.at(position);
    return mapClient.at(position);
}
