#ifndef DATE_H
#define DATE_H

#include <iostream>
#include <sstream>

using namespace std;

class Date
{
  private:
     double Day;
     double Month;
     double Year;

  public:
     Date(double=0, double=0, double=0);
     virtual ~Date();

     double getDay() { return Day;}
     void setDay(double dDay) {Day = dDay;}
     double getMonth() {return Month;}
     void setMonth(double dMonth) {Month = dMonth;}
     double getYear() {return Year;}
     void setYear(double dYear) {Year = dYear;}

     string Infos();
};
#endif

