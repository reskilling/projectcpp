#ifndef PROFESSIONNEL_H
#define PROFESSIONNEL_H
#include "Client.h"
/*------------------*/
#include <iostream>
#include <sstream>
#include <string>
#include "AdresseSiege.h"
/*------------------*/

class Professionnel : virtual public Client
{
  private:
    string Siret;
    string SatutJuridique;
    AdresseSiege *AdressePS=nullptr;

  public:
                  
    Professionnel(int=0, string="\0", string="\0", string="\0", int=0, string="\0", string="\0", string="0", string="\0", string="\0", string="\0", int=0, string="\0");
    virtual ~Professionnel();

    string getSiret(){ return Siret;}
    void setSiret(string dSiret)
    {
        if (dSiret.length() > 14)
        {
          throw MonException(5);
        }
       Siret = dSiret;
    }
   
    string getSatutJuridique() {return SatutJuridique;}
    void setSatutJuridique(string dSatutJuridique)
    {
      if (dSatutJuridique!="SARL" && dSatutJuridique!="SAS" && dSatutJuridique!="SA" && dSatutJuridique!="EURL")
           throw MonException(6);
      SatutJuridique = dSatutJuridique;
    }
    
    AdresseSiege getAdressePS();
    void setAdressePS(string="\0", string="\0", int=0, string="\0");

    void Infos() override;
};
#endif