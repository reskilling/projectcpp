#include "Adresse.h"

Adresse::Adresse(string L, string CM, int CD, string V)
{
    setLibelle(L);
    setComplement(CM);
    setCode(CD);
    setVille(V);
}
Adresse::~Adresse()
{
    cout << "Constructor (Adresse) : " << endl;
}

string Adresse::Infos()
{
    ostringstream oss;
    oss << "Libelle : " << getLibelle() << endl;
    oss << "Complement : " << getComplement() << endl;
    oss << "Ville : " << getVille() << endl;
    oss << "Code Psotale : " << getCode() << endl;

    return oss.str();
}