#ifndef GESTIONCLIENT_H
#define GESTIONCLIENT_H

#include <map>
//#include <vector>
#include "Client.h" 

class gestionClient
{

    private:

//        vector<Client*> vecClient;
    	map<int, Client*> mapClient; 


    public:
        gestionClient();
        virtual ~gestionClient();

        int GetnbClient();


        void Ajouter(Client*);
        void AfficherTout();

        Client *getClient(int);
//        Client getClient(int);
};
#endif 