#include "Professionnel.h"
                                                                                               
Professionnel::Professionnel(int I, string N, string L, string CM, int CD, string V, string E, string SR, string SJ, string LS, string CMS, int CDS, string VS)
    : Client(I, N, L, CM, CD, V, E)
{
   setSiret(SR);
   setSatutJuridique(SJ);
   AdressePS = new AdresseSiege(LS, CMS, CDS, VS);
}

Professionnel::~Professionnel()
{
    cout << "Constructor (Professionnel) : " << endl;
        if (AdressePS != nullptr)
      {
        delete AdressePS;
      }
}

void Professionnel::Infos()
{
   Client::Infos();
   cout << "Siret : " << getSiret() << endl;
   cout << "Satut Juridique: " << getSatutJuridique() << endl;
   cout << (*AdressePS).Infos();
}

AdresseSiege Professionnel::getAdressePS()
{
   return *AdressePS;
}

void Professionnel::setAdressePS(string newLS, string newCMS, int newCDS, string newVS)
{
   (*AdressePS).setLibelleSiege(newLS);
   (*AdressePS).setComplementSiege(newCMS);
   (*AdressePS).setCodeSiege(newCDS);
   (*AdressePS).setVilleSiege(newVS);
}