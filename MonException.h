#ifndef MONEXCEPTION_H
#define MONEXCEPTION_H

#include <exception>

#include <iostream>

using namespace std;

class MonException:public exception
{
    private:
        mutable string message;
        int codeErr;

    public:
        MonException(int) throw();
        virtual ~MonException() throw();

        string Getmessage() const ;

        const char* what() const throw() ;

};
#endif