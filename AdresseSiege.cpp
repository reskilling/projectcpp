#include "AdresseSiege.h"

AdresseSiege::AdresseSiege(string LS, string CMS, int CDS, string VS)
{
    setLibelleSiege(LS);
    setComplementSiege(CMS);
    setCodeSiege(CDS);
    setVilleSiege(VS);
}
AdresseSiege::~AdresseSiege()
{
    cout << "Constructor (AdresseSiege) : " << endl;
}

string AdresseSiege::Infos()
{
    ostringstream oss;
    oss << "Libelle Siege : " << getLibelleSiege() << endl;
    oss << "Complement Siege : " << getComplementSiege() << endl;
    oss << "Code Psotale Siege : " << getCodeSiege() << endl;
    oss << "Ville Siege : " << getVilleSiege() << endl;
    return oss.str();
}