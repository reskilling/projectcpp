#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <sstream>
#include <string>
#include "Adresse.h"
#include "MonException.h"

class Client
{
   private:
     int Identifiant;
     string Nom;
     Adresse *AdressePostale=nullptr;
     string Email;

   public:
      Client(int=0, string="\0", string="\0", string="\0", int=0, string="\0", string="\0");
      ~Client();

     int getIdentifiant() { return Identifiant;}
     void setIdentifiant(int dIdentifiant) { Identifiant = dIdentifiant;}

     string getNom() { return Nom;}
     void setNom(string);
     
     Adresse getAdressePostale();
     void setAdressePostale(string="\0", string="\0", int=0, string="\0");

     string getEmail() { return Email;}
     void setEmail(string dEamil)
     {
       int found = dEamil.find('@');
       if(found > dEamil.length())
           throw MonException(2);
           Email = dEamil;
     }

     virtual void Infos();
};

#endif