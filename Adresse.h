#ifndef ADRESSE_H
#define ADRESSE_H

#include <iostream>
#include <sstream>

using namespace std;

class Adresse 
{
   private:
      string Libelle;
      string Complement;
      int Code;
      string Ville;
   public:
   
     Adresse(string="\0", string="\0", int=0, string="\0");
     virtual ~Adresse();
     
     string getLibelle() { return Libelle;}
     void setLibelle(string dLibelle) { Libelle = dLibelle; }

     string getComplement() { return Complement;}
     void setComplement(string dComplement) { Complement = dComplement; }
    
     int getCode() { return Code;}
     void setCode(int dCode) { Code = dCode; }

     string getVille() { return Ville;}
     void setVille(string dVille) { Ville = dVille; }
     
     string Infos();
};
#endif