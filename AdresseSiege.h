#ifndef ADRESSESIEGE_H
#define ADRESSESIEGE_H

#include <iostream>
#include <sstream>

using namespace std;

class AdresseSiege
{
   private:
      string LibelleSiege;
      string ComplementSiege;
      int CodeSiege;
      string VilleSiege;
   public:
   
     AdresseSiege(string="\0", string="\0", int=0, string="\0");
     virtual ~AdresseSiege();
     
     string getLibelleSiege() { return LibelleSiege;}
     void setLibelleSiege(string dLibelle) { LibelleSiege = dLibelle; }

     string getComplementSiege() { return ComplementSiege;}
     void setComplementSiege(string dComplement) { ComplementSiege = dComplement; }
    
     int getCodeSiege() { return CodeSiege;}
     void setCodeSiege(int dCode) { CodeSiege = dCode; }

     string getVilleSiege() { return VilleSiege;}
     void setVilleSiege(string dVille) { VilleSiege = dVille; }
     
     string Infos();
};
#endif