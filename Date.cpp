#include "Date.h"


Date::Date(double J, double M, double Y)
{
    setDay(J);
    setMonth(M);
    setYear(Y);
}

Date::~Date()
{
  cout << "Constructor (Date) : " << endl;
}

string Date::Infos()
{
    ostringstream oss;
    oss << "Date de naissance : " << getDay() << "/" << getMonth() << "/" << getYear() << endl;
    return oss.str();
}