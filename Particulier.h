#ifndef PARTICULIER_H
#define PARTICULIER_H
#include "Client.h"
/*------------------*/

#include <iostream>
#include <sstream>
#include <string>
#include "Date.h"
/*------------------*/

class Particulier : virtual public Client
{
  private:
     Date *DatedeNaisaance=nullptr; //Ajouté pour afficher la date 
     string Prenom;
     string Sexe;
     
  public:
     Particulier(int=0, string="\0", string="\0", string="\0", int=0, string="\0", string="\0", double=0, double=0, double=0, string="\0", string="\0");
     virtual ~Particulier();

     Date getDatedeNaissace();
     void setDatedeNaisaance(double=0, double=0, double=0); 

     string getPrenom() {return Prenom;}
     void setPrenom(string dPrenom)
     {
        if (dPrenom.length() > 50)
        {
          throw MonException(1);
        }
        
        Prenom = dPrenom;
     }
   
    string getSexe(){ return Sexe;}
    void setSexe(string dSexe)
    {
       if (dSexe!="F" && dSexe!="M")
           throw MonException(4);
        Sexe = dSexe;
    }

   void Infos() override;
};
#endif
