#include "MonException.h"

MonException::MonException(int err)throw()
    :exception()
{
    codeErr=err;
}

MonException::~MonException() throw()
{
    //dtor
}

string MonException::Getmessage() const
{
    switch (this->codeErr)
    {
        case 1:
            message = "Nom (Length > 50) Trop Longue !";
            break;
        case 2:
            message = "Email manque le @ !";
            break;
        case 3:
            message = "Prenom (Length > 50) Trop Longue !";
            break;
        case 4:
            message = "Sexe différent (F / M)  !";
            break;
        case 5:
            message = "Siret dépasse 14 chiffres !";
            break;
        case 6:
            message = "Statut Juridique différent de (SARL, SA, SAS, EURL) !";
            break;
        default:
            message = "Autre Erreur !";
            break;
    }
    return message;
}

const char* MonException::what() const throw()
{
    Getmessage();
    return message.c_str();
}